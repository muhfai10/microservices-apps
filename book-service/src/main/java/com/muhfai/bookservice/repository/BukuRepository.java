package com.muhfai.bookservice.repository;

import com.muhfai.bookservice.model.Buku;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BukuRepository extends JpaRepository<Buku, Long> {
}
