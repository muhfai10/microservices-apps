package com.muhfai.bookservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BukuRequest {
    private String kodeBuku;
    private String judul;
    private String pengarang;
    private String penerbit;
    private String tahun;
    private BigDecimal harga;
    private String deskripsi;
}
