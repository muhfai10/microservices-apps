package com.muhfai.bookservice.controller;

import com.muhfai.bookservice.dto.BukuRequest;
import com.muhfai.bookservice.dto.BukuResponse;
import com.muhfai.bookservice.service.BukuService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/buku")
@RequiredArgsConstructor
public class BukuController {

    private final BukuService bukuService;

    @PostMapping("/insert")
    @ResponseStatus(HttpStatus.CREATED)
    public void insertBuku(@RequestBody BukuRequest bukuRequest){
        bukuService.insertBuku(bukuRequest);
    }

    @GetMapping("/all-buku")
    public List<BukuResponse> findAllBuku(){
        return bukuService.findAllBuku();
    }
}
