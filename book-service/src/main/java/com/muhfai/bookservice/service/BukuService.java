package com.muhfai.bookservice.service;

import com.muhfai.bookservice.dto.BukuRequest;
import com.muhfai.bookservice.dto.BukuResponse;
import com.muhfai.bookservice.model.Buku;
import com.muhfai.bookservice.repository.BukuRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Component
@RequiredArgsConstructor
@Slf4j
public class BukuService {

    private final BukuRepository bukuRepository;

    public void insertBuku(BukuRequest bukuRequest){
        Buku buku = Buku.builder()
                .kodeBuku(bukuRequest.getKodeBuku())
                .judul(bukuRequest.getJudul())
                .pengarang(bukuRequest.getPengarang())
                .penerbit(bukuRequest.getPenerbit())
                .tahun(bukuRequest.getTahun())
                .harga(bukuRequest.getHarga())
                .deskripsi(bukuRequest.getDeskripsi()).build();

        bukuRepository.save(buku);
        log.info("Buku {} is saved", buku.getId());
    }

    public List<BukuResponse> findAllBuku(){
        List<Buku> bukuList = bukuRepository.findAll();

        return bukuList.stream().map(buku -> mapToBukuResponse(buku)).collect(Collectors.toList());
    }

    private BukuResponse mapToBukuResponse(Buku buku) {
        return BukuResponse.builder()
                .id(buku.getId())
                .kodeBuku(buku.getKodeBuku())
                .judul(buku.getJudul())
                .pengarang(buku.getPengarang())
                .penerbit(buku.getPenerbit())
                .tahun(buku.getTahun())
                .harga(buku.getHarga())
                .deskripsi(buku.getDeskripsi()).build();
    }
}
