package com.muhfai.bookservice.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "m_buku")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Buku {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "kode_buku")
    private String kodeBuku;

    @Column(name = "judul")
    private String judul;

    @Column(name = "pengarang")
    private String pengarang;

    @Column(name = "penerbit")
    private String penerbit;

    @Column(name = "tahun")
    private String tahun;

    @Column(name = "harga")
    private BigDecimal harga;

    @Column(name = "deskripsi")
    private String deskripsi;
}
