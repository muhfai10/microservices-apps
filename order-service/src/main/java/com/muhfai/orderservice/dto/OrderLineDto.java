package com.muhfai.orderservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderLineDto {
    private Long id;
    private String orderCode;
    private String kodeBuku;
    private BigDecimal harga;
    private Integer jumlah;
}
