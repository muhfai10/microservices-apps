package com.muhfai.orderservice.controller;

import com.muhfai.orderservice.dto.OrderRequest;
import com.muhfai.orderservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/order")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @PostMapping("add-order")
    @ResponseStatus(HttpStatus.CREATED)
    public void addOrder(@RequestBody OrderRequest orderRequest){
        orderService.addOrder(orderRequest);
    }
}
