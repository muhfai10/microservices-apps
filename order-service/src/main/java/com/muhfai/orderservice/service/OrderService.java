package com.muhfai.orderservice.service;

import com.muhfai.orderservice.dto.InventoryResponse;
import com.muhfai.orderservice.dto.OrderLineDto;
import com.muhfai.orderservice.dto.OrderRequest;
import com.muhfai.orderservice.model.Order;
import com.muhfai.orderservice.model.OrderLine;
import com.muhfai.orderservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional
public class OrderService {

    private final OrderRepository orderRepository;
    private final WebClient.Builder webClientBuilder;

    public void addOrder(OrderRequest orderRequest){
        Order order = new Order();
        order.setOrderNumber(UUID.randomUUID().toString());

        List<OrderLine> orderLineList = orderRequest.getOrderLineDtoList()
                .stream()
                .map(orderLineDto -> mapToDto(orderLineDto))
                .collect(Collectors.toList());

        order.setOrderLineList(orderLineList);

        List<String> kodeBukuList = order.getOrderLineList().stream()
                        .map(OrderLine::getKodeBuku)
                                .collect(Collectors.toList());

        InventoryResponse[] inventoryResponseArr = webClientBuilder.build().get()
                        .uri("http://inventory-service/api/inventory/isinstock",
                                uriBuilder -> uriBuilder.queryParam("kodeBuku", kodeBukuList).build())
                                .retrieve()
                                        .bodyToMono(InventoryResponse[].class)
                                                .block();

        Boolean isInStock = Arrays.stream(inventoryResponseArr)
                .allMatch(inventoryResponse -> inventoryResponse.getIsInStock());

        if(isInStock){
            orderRepository.save(order);
            log.info("Order {} is saved", order.getOrderNumber() );
        }else {
            throw new IllegalArgumentException("Product is not in stock");
        }



    }

    private OrderLine mapToDto(OrderLineDto orderLineDto) {
        OrderLine orderLine = new OrderLine();
        orderLine.setOrderCode(orderLineDto.getOrderCode());
        orderLine.setKodeBuku(orderLineDto.getKodeBuku());
        orderLine.setHarga(orderLineDto.getHarga());
        orderLine.setJumlah(orderLineDto.getJumlah());
        return orderLine;
    }
}
