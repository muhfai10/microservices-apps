package com.muhfai.inventoryservice.controller;

import com.muhfai.inventoryservice.dto.InventoryRequest;
import com.muhfai.inventoryservice.dto.InventoryResponse;
import com.muhfai.inventoryservice.service.InventoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/inventory")
@RequiredArgsConstructor
public class InventoryController {

    private final InventoryService inventoryService;

    @GetMapping("/isinstock")
    @ResponseStatus(HttpStatus.OK)
    public List<InventoryResponse> isInStock(@RequestParam List<String> kodeBuku) {
        return inventoryService.isInStock(kodeBuku);
    }

    @PostMapping("/insert")
    @ResponseStatus(HttpStatus.CREATED)
    public void insertInventory(@RequestBody InventoryRequest inventoryRequest){
        inventoryService.insertInventory(inventoryRequest);
    }
}
