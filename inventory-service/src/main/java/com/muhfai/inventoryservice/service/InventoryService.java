package com.muhfai.inventoryservice.service;

import com.muhfai.inventoryservice.dto.InventoryRequest;
import com.muhfai.inventoryservice.dto.InventoryResponse;
import com.muhfai.inventoryservice.model.Inventory;
import com.muhfai.inventoryservice.repository.InventoryRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class InventoryService {

    private final InventoryRepository inventoryRepository;

    @Transactional(readOnly = true)
    public List<InventoryResponse> isInStock(List<String> kodeBuku){
        return inventoryRepository.findByKodeBukuIn(kodeBuku)
                .stream()
                .map(inventory ->
                        InventoryResponse.builder()
                        .kodeBuku(inventory.getKodeBuku())
                        .isInStock(inventory.getQuantity() > 0)
                        .build()).collect(Collectors.toList());
    }

    public void insertInventory(InventoryRequest inventoryRequest){
        Inventory inventory = Inventory.builder()
                .kodeBuku(inventoryRequest.getKodeBuku())
                .quantity(inventoryRequest.getQuantity())
                .build();

        inventoryRepository.save(inventory);
        log.info("Inventory {} is saved", inventory.getId());
    }
}
